
This is a repository for my binaryweather project.
binaryweather is a relatively simple project that if installed on a web server
allows users to be able to get the weather by location.
The front end is completely written in AngularJS 2.x
and the backend is written in php and uses a restful webservice 
to supply the front end with data.
The weather data it's self is derived from a weather API supplied
by wunderground.

