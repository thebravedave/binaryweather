<?php
/***************************************************************************************
 ****************************************************************************************
 *     All code shown copyright of Binary Web Design LLC. Copyright 2013. ***************
 ****************************************************************************************
 ***************************************************************************************/

class Location{
    public $state = "OR";
    public $city = "Eugene";
    public $zipCode = "97402";
}

class Success{
    public $success = true;
    public $message = "";
}
class Webcams{
    public $webcamArray1 =[];
    public $webcamArray2 = [];
    public $webcamArray3 = [];
}
class Slide{
    public $image;
    public $text;
}

?>
