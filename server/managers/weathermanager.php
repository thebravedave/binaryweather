<?php

class WeatherManager{
    public $baseUrl = "http://api.wunderground.com/api/";
    function __construct(){
        $key = WeatherUtil::getWeatherKey();
        $this->baseUrl = "http://api.wunderground.com/api/" . $key . "/";
    }
    public function getTenDayWeatherForecast($location){

        $locationString = $location->state . "/" . $location->city . ".json";
        $serviceUrl = $this->baseUrl . "/forecast10day/q/" . $locationString;
        $curl = curl_init($serviceUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return null;
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            return null;
        }
        if(!isset($decoded->forecast)){
            return null;
        }
        return $decoded->forecast;
    }
    public function getSatelliteImagery($location){
        $locationString = $location->state . "/" . $location->city . ".json";
        $serviceUrl = $this->baseUrl . "/satellite/q/" . $locationString;
        $curl = curl_init($serviceUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return null;
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        return $decoded->satellite;
    }

    public function getWebCameraImages($location){

        $locationString = $location->state . "/" . $location->city . ".json";
        $serviceUrl = $this->baseUrl . "/webcams/q/" . $locationString;
        $curl = curl_init($serviceUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return null;
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $webcams = $decoded->webcams;
        $webcams1 = [];
        $webcams2 = [];
        $webcams3 = [];
        $webcams1Bool = true;
        $webcams2Bool = false;
        $webcams3Bool = false;

        foreach($webcams as $id=>$value){
            $slide = new Slide();
            $slide->image = $value->CURRENTIMAGEURL;
            $slide->text = $value->city;
            if($value->linktext != ""){
                $slide->text .= " , " . $value->linktext;
            }
            if($webcams1Bool){
                $webcams1[sizeof($webcams1)] = $slide;
                $webcams1Bool = false;
                $webcams2Bool = true;
            }
            elseif($webcams2Bool){
                $webcams2[sizeof($webcams2)] = $slide;
                $webcams2Bool = false;
                $webcams1Bool = false;
                $webcams3Bool = true;
            }
            elseif($webcams3Bool){
                $webcams3[sizeof($webcams3)] = $slide;
                $webcams3Bool = false;
                $webcams1Bool = true;
            }
        }
        $webcams = new Webcams();
        $webcams->webcamArray1 = $webcams1;
        $webcams->webcamArray2 = $webcams2;
        $webcams->webcamArray3 = $webcams3;
        return $webcams;
    }

    public function getLocationInformation($zipcode){

        $locationString = $zipcode . ".json";
        $serviceUrl = $this->baseUrl . "/geolookup/q/" . $locationString;
        $curl = curl_init($serviceUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return null;
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            return null;
        }
        if(!isset($decoded->location)){
            return null;
        }
        return $decoded->location;
    }
}

























