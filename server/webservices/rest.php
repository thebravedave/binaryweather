<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');



session_start();

include_once("../dto/datatransferobjects.php");
include_once("../managers/weathermanager.php");
include_once("../util/weatherutil.php");


require '../lib/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'debug' => true
));

$app->get('/forecast/10day/:state/:city', function($state, $city) use($app) {

    $weatherManager = new WeatherManager();


    $location = new Location();
    $location->city = $city;
    $location->state = $state;

    $forecast = $weatherManager->getTenDayWeatherForecast($location);
    $success = new Success();
    $success->message = $forecast;
    $json = json_encode($success);
    echo $json;

}); //forecast/10day/:state/:city'



$app->get('/forecast/satellite/:state/:city', function($state, $city) use($app) {

    $weatherManager = new WeatherManager();

    $location = new Location();
    $location->city = $city;
    $location->state = $state;

    $satelliteImagery = $weatherManager->getSatelliteImagery($location);
    $success = new Success();
    $success->message = $satelliteImagery;
    $json = json_encode($success);
    echo $json;

}); //forecast/satellite/:state/:city'


$app->get('/forecast/webcams/:state/:city', function($state, $city) use($app) {

    $weatherManager = new WeatherManager();

    $location = new Location();
    $location->city = $city;
    $location->state = $state;

    $forecast = $weatherManager->getWebCameraImages($location);
    $success = new Success();
    $success->message = $forecast;
    $json = json_encode($success);
    echo $json;

}); //forecast/webcams/:state/:city'


$app->get('/forecast/geolocator/zipcode/:zipcode', function($zipcode) use($app) {

    $weatherManager = new WeatherManager();
    $forecast = $weatherManager->getLocationInformation($zipcode);
    $success = new Success();
    if($forecast == null){
        $success->success = false;
    }
    else{
        $success->message = $forecast;
    }

    $json = json_encode($success);
    echo $json;

}); //forecast/geolocator/zipcode/:zipcode


$app->run();

