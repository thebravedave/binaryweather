<?php

class WeatherUtil{

    static function getWeatherKey()
    {
        $locationName = "../conf/apiconfiguration.php";
        try {
            $apiKeyFile = file($locationName);
        } catch (Exception $e) {
            return null;
        }
        $apiString = "";



        foreach ($apiKeyFile as $key => $value) {
            if (strpos($value, "key") !== false)
                $apiString = $value;
        }
        if ($apiString == "")
            return null;

        $keyArray = explode("=", $apiString);

        if (sizeof($keyArray) != 2)
            return null;
        $apiKeyValue = $keyArray[1];


        $apiKeyValue = trim($apiKeyValue);

        return $apiKeyValue;

    }
}

