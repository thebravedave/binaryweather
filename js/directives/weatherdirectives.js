'use strict';

angular.module('WeatherDirectives', ['ui.bootstrap'])
.directive('tenDayWeatherForecast', function() {
        return {
            restrict: "E",
            scope: {
                forecast: '=',
                satelliteData: '=',
                webcamData: '='
            },
            link: function (scope, element, $location) {},
            controller: function($rootScope, $scope, $location){
                init();
                function init(){
                    $scope.myInterval = 10000;
                }
            },
            templateUrl:"./partials/directivetemplates/tendayweatherforecast.html"
        }
})
.directive('weatherDirectiveContainer', function() {
    return {
        restrict: "E",
        scope: {},
        link: function (scope, element, $location) {},
        controller: function($rootScope, $scope, $location, WeatherService){
            init();
            function init(){
                $scope.geographicalInput = "";
                initializeEventListeners();
            }
            function initializeEventListeners(){
                $scope.$watch('geographicalInput', function(){
                    var isnum = /^\d+$/.test($scope.geographicalInput);
                    if(isnum && $scope.geographicalInput.toString().length == 5){
                        WeatherService.getGeographicalInformationByZipcode($scope.geographicalInput).success(function(data){
                            if(data.success == true){
                                $scope.locationError = false;
                                $scope.userState = data.message.state;
                                $scope.userCity = data.message.city;
                                getWeatherData();
                                getSatelliteData();
                                getWebcamData();
                            }
                            else{
                                $scope.locationError = true;
                            }
                        }).error(function(){
                            $scope.locationError = true;
                        })
                    }
                    else{
                        $scope.locationError = true;
                    }
                });
            }
            function getWeatherData(){
                if($scope.userState != undefined && $scope.userCity != undefined){
                    WeatherService.getTenDayWeatherForecast($scope.userState, $scope.userCity).success(function(data){
                        if(data.success == true){
                            $scope.forecast = data.message;
                        }
                    }).error(function(){

                    })
                }
            }
            function getSatelliteData(){
                if($scope.userState != undefined && $scope.userCity != undefined){
                    WeatherService.getSatelliteData($scope.userState, $scope.userCity).success(function(data){
                        if(data.success == true){
                            $scope.satelliteData = data.message;
                        }
                    }).error(function(){

                    })
                }
            }
            function getWebcamData(){
                if($scope.userState != undefined && $scope.userCity != undefined){
                    WeatherService.getWebcamData($scope.userState, $scope.userCity).success(function(data){
                        if(data.success == true){
                            $scope.webcamData = data.message;
                        }
                    }).error(function(){

                    })
                }
            }
        },
        templateUrl:"./partials/directivetemplates/weatherdirectivecontainer.html"
    }
})
