'use strict';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////M O D U L E   A P P    R E G I S T R A T I O N/////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var weatherApp = angular.module('weatherApp', [
    'ngRoute',
    'WeatherController',
    'WeatherServices',
    'WeatherDirectives',
    'ngTouch'

]);

weatherApp.filter("sanitize", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////          M O D U L E   R O U T E R           /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
weatherApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode(false);
        $routeProvider.
            when('/forecast', {
                templateUrl: 'partials/controllertemplates/weatherforecast.html',
                controller: 'WeatherController',
                access: {
                    name: "login",
                    isFree: true
                }
            }).
            otherwise({
                redirectTo: '/forecast/'
            });
    }]
);





