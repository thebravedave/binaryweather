'use strict';

/* Services */
//////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////W E A T H E R      S E R V I C E S /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

var categoryServices = angular.module('WeatherServices', []);
categoryServices.factory('WeatherService', function($http) {
    return {
        getGeographicalInformationByZipcode: function(zipcode){
            return $http.get('./server/webservices/rest.php/forecast/geolocator/zipcode/' + zipcode);
        },
        getTenDayWeatherForecast: function(state, city){
            return $http.get('./server/webservices/rest.php/forecast/10day/' + state + '/' + city);
        },
        getSatelliteData: function(state, city){
            return $http.get('./server/webservices/rest.php/forecast/satellite/' + state + '/' + city);
        },
        getWebcamData: function(state, city){
            return $http.get('./server/webservices/rest.php/forecast/webcams/' + state + '/' + city);
        }
    }
});
